var fileSystem;
function err(e){console.log(e)}
function removeFile(fs,fname){
	fs.root.getFile(fname, {create: false}, function(fileEntry) {
		fileEntry.remove(function() {
		  console.log('File removed.');
		},err);
	},err)
}
function clearAll(){
	$("#directory a").each(function(){
		console.log($(this).text())
		removeFile(fileSystem,$(this).text())
	})
}
function readFile(fs,fname){
	fs.root.getFile(fname,{},function(f){
		f.file(function(fl){
			console.log(fl)
			r=new FileReader()
			r.readAsText(fl);
			r.onloadend=function(){
				console.log(r.result)
			}
		})
	})
}
function convert(input) {
    output = "";
    for (i=0; i < input.length; i++) {
        output +=input[i].charCodeAt(0).toString(16) + " ";
    }
return output
}
(function(){
	var Renderer = function(canvas){
		var canvas = $(canvas).get(0)
		var ctx = canvas.getContext("2d");
		var gfx = arbor.Graphics(canvas);
		var particleSystem

		var renderSpace = {
			init:function(system){
				particleSystem = system;
				particleSystem.screenSize(canvas.width, canvas.height) ;
				particleSystem.screenPadding(80);
				renderSpace.initMouseHandling();
			},
			redraw:function(){
				ctx.fillStyle = "rgba(245,245,245,0.6)";
				ctx.fillRect(0,0, canvas.width, canvas.height);
				particleSystem.eachEdge(function(edge, pt1, pt2){
					if(edge.data.hidden) return;
					ctx.strokeStyle = "rgba(238,138,17,0.5)";
					ctx.lineWidth = 3;
					ctx.beginPath();
					ctx.moveTo(pt1.x, pt1.y);
					ctx.lineTo(pt2.x, pt2.y);
					ctx.stroke();
				});
				particleSystem.eachNode(function(node, pt){
					if(node.data.hidden) return;
					var w = node.data.radius;
					ctx.fillStyle = node.data.color;
					gfx.oval(pt.x-w/2, pt.y-w/2, w,w, {fill:ctx.fillStyle});
					ctx.font = "11px Helvetica";
					ctx.textAlign = "center";
					ctx.fillStyle = '#333333';
					ctx.fillText(node.data.label, pt.x, pt.y+4);
				});
			},
			initMouseHandling:function(){
				var dragged = null;
				var handler = {
					clicked:function(e){
						var pos = $(canvas).offset();
						_mouseP = arbor.Point(e.pageX-pos.left, e.pageY-pos.top);
						dragged = particleSystem.nearest(_mouseP);
						if (dragged && dragged.node !== null){
							dragged.node.fixed = true;
						}
						$(canvas).bind('mousemove', handler.dragged);
						$(window).bind('mouseup', handler.dropped);
						return false;
					},
					dragged:function(e){
						var pos = $(canvas).offset();
						var s = arbor.Point(e.pageX-pos.left, e.pageY-pos.top);
						if (dragged && dragged.node !== null){
							var p = particleSystem.fromScreen(s);
							dragged.node.p = p;
						}
						return false;
					},
					dropped:function(e){
						if (dragged===null || dragged.node===undefined) return;
						if (dragged.node !== null) dragged.node.fixed = false;
						dragged.node.tempMass = 1000;
						dragged = null;
						$(canvas).unbind('mousemove', handler.dragged);
						$(window).unbind('mouseup', handler.dropped);
						_mouseP = null;
						return false;
					}
				};
				$(canvas).mousedown(handler.clicked);
			}
		};
		return renderSpace;
	};
	var Queue = function(){
		this.data = [];
		this.enqueue = function(x){
			this.data.push(x);
			return;
		}
		this.size  = function(){
			return this.data.length;
		}
		this.dequeue = function(){
			return this.data.shift();
		}
	};
	function startFileService(){
		window.webkitRequestFileSystem(window.TEMPORARY,300*1024*1024,function(fs){
			//Requesting 300MB temporary storage
			fileSystem = fs;
			getFiles(fileSystem);
		},function(err){console.log(err)});
	}
	
	var downloads = {};
	function getFiles(fs) {
		console.log("Getting files...");
		$("#directory").html("");
		var dirReader = fs.root.createReader();
		var entries = [];
		var readEntries = function() {
			dirReader.readEntries(function(results){
				if (!results.length){
					for(i in entries)
						if(entries[i]){
							var status = "";
							if(downloads[entries[i].name])
								status="incomplete"
							$("#directory").append("<a target='_blank' href='filesystem:http://localhost:8080/temporary/"+entries[i].name+"' class='"+status+"'>"+entries[i].name+"</a><br/>");
						}
					//setTimeout(function(){getFiles(fs)},1000);
				}
				else{
					entries = entries.concat(Array.prototype.slice.call(results||[],0));
					readEntries();
				}
			});
		};
		readEntries();
	}
	function randrange(a,b){
		return Math.ceil(Math.random()*b)+a;
	}
	var color = {
		colors: [
		  "#fbb59c",
		  "#fbe59c",
		  "#e2fb9c",
		  "#fb9ce2",
		  "#96b7ff",
		  "#ffaf96",
		  "#d11b67",    
		],
		random: function(){
			return this.colors[randrange(-1,this.colors.length)];
		}
	};
	var socket = io.connect('http://localhost:8080');
	$(document).ready(function(){
		var myName,myGroup;
		function joinGroup(groupID,Name){
			socket.emit('join',{id:groupID,name:Name});
			startFileService();
		}
		function uploadError(e){
			$("#files").val("");	
			$("#up-stat").parent().parent().prepend("<span class='glyphicon glyphicon-remove pull-right'></span>")
			$("#up-stat").parent().remove()
		}
		$("#select-nick").click(function(){
			if($("#nickname").val()=="") return;
			myName = $("#nickname").val();
			socket.emit('set-nick',{name:myName});
			socket.on('set-nick-result',function(res){
				if(res.status){
					$("#nick").hide();
					$("#group").show()
				}
				else{
					$('#error-div').addClass('label')
					$('#error-div').addClass('label-danger')
					$('#error-div').text("Nickname already in use.");
				}
			});
		});
		$("#select-group").click(function(){
			if($("#groupId").val()=="") return;
			myGroup = $("#groupId").val();
			$("#group").hide();
			$("#app").show();
			joinGroup(myGroup,myName);
		});
		//Graph 
		var sys = arbor.ParticleSystem();
		sys.parameters({stiffness:90, repulsion:1000, gravity:false, dt:0.015});
		sys.renderer = Renderer("#viewport");
		
		var myNode;
		
		sys.addNode('a',{color:"#619b45",radius:35,hidden:1});
		sys.addNode('b',{color:"#21526a",radius:35,hidden:1});
		sys.addEdge('a','b',{hidden:true});
		$("#delete").click(function(){
			function remove(fs,fname){
				fs.root.getFile(fname, {create: false}, function(fileEntry) {
					fileEntry.remove(function() {
						console.log('File removed.');
					},err);
				},err);
			}
			$("#directory a").each(function(){remove(fileSystem,$(this).text())})
			getFiles(fileSystem);
		});
		$("#send").click(function(){
			var file = $("#files")[0].files[0];
			if(!file) return;
			$("#files,#send").attr('disabled','disabled');
			const chunkSize = 128*1024;
			const totalSize = file.size;
			const throughput= 2;
			var start = 0;
			var totalFragments = Math.ceil(totalSize/chunkSize);
			var idx=1;
			var fileName = file.name.replace(/[ ]/g,'-')
			console.log("Sending file in " + totalFragments+" fragments...");
			$("#view").prepend("<tr class='status-bar'><td><p>Uploading "+fileName+"</p><div class='progress progress-striped active'><div class='progress-bar up-stat' id='up-stat'></div></div></td></tr>");
			
			fileSystem.root.getFile(fileName,{create:true},function(newFile){
				getFiles(fileSystem);
				downloads[fileName]=1;
				var fileRead = setInterval(function(){
					if(!file){
						clearInterval(fileRead);
						return;
					}
					if(start > totalSize){
						clearInterval(fileRead);
						console.log("File Sent!");
						$("#files,#send").removeAttr('disabled');
						$("#up-stat").parent().parent().prepend("<span class='glyphicon glyphicon-ok pull-right'></span>")
						$("#up-stat").parent().remove()
						return;
					}
					(function(fragpos,start){
						var reader = new FileReader();
						reader.readAsBinaryString(file.slice(start,start+chunkSize,"application/octet-stream"));
						reader.onload = function(){
							$("#up-stat").css('width',fragpos*100/totalFragments+'%');
							fragmentQueue.enqueue({"fileName":fileName,"fileContent":reader.result,"fileFrag":fragpos,"totalFrags":totalFragments});
							if(fragpos==1)
								socket.emit('fragments',{data:reader.result,frag:fragpos+"/"+totalFragments,type:(file.type||'application/octet-stream'),name:fileName})
							else
								socket.emit('fragments',{data:reader.result,frag:fragpos+"/"+totalFragments,name:fileName})
						}
						reader.onerror = function(){
							$("#up-stat").parent().parent().prepend("<span class='glyphicon glyphicon-remove pull-right'></span>")
							$("#up-stat").parent().remove()
						}
					})(idx,start);
					start += chunkSize;
					idx+=1;
				},1000/throughput);
			},err);	
		});

		var fragmentQueue = new Queue();
		setInterval(function(){
			if(fragmentQueue.size() > 0){
				var fragment = fragmentQueue.dequeue();
				if(!downloads[fragment.fileName]) return;
				const chunkSize = 128*1024;
				fileSystem.root.getFile(fragment.fileName,{},function(file){
					file.createWriter(function(fileWriter){
						fileWriter.seek((fragment.fileFrag-1)*chunkSize);
						fileWriter.onwriteend = function(e){
							$("#file-"+downloads[fragment.fileName]).css('width',fragment.fileFrag*100/fragment.totalFrags+'%');
							if(fragment.fileFrag == fragment.totalFrags){
								$("#file-"+downloads[fragment.fileName]).parent().parent().prepend("<span class='glyphicon glyphicon-ok pull-right'></span>");
								$("#file-"+downloads[fragment.fileName]).parent().remove();
								downloads[fragment.fileName] = 0;
								getFiles(fileSystem);
							}
						}
						fileWriter.onerror = function(e){
							$("#file-"+downloads[fragment.fileName]).parent().parent().prepend("<span class='glyphicon glyphicon-remove pull-right'></span>");
							$("#file-"+downloads[fragment.fileName]).parent().remove();
							downloads[fragment.fileName] = 0;
							removeFile(fileSystem,fragment.fileName);
							getFiles(fileSystem);
						}
						var byteArray = new Uint8Array(fragment.fileContent.length);
						for(var i=0;i<fragment.fileContent.length;i++)
							byteArray[i] = fragment.fileContent.charCodeAt(i) & 0xff;
						fileWriter.write(new Blob([new DataView(byteArray.buffer)]));
					});
				},function(e){
					$("#file-"+downloads[fragment.fileName]).parent().parent().prepend("<span class='glyphicon glyphicon-remove pull-right'></span>");
					$("#file-"+downloads[fragment.fileName]).parent().remove();
					downloads[fragment.fileName] = 0;
				});
			}
		},100);
		socket.on('receive-new',function(fileDetails){
			console.log("Creating file "+fileDetails.name);
			downloads[fileDetails.name] = randrange(1000,100000000);
			$("#view").prepend("<tr><td><div>Receiving "+fileDetails.name+"</div><div class='progress progress-striped active'><div class='progress-bar progress-bar-success down-stat' id='file-"+downloads[fileDetails.name]+"'></div></td></tr>");
			fileSystem.root.getFile(fileDetails.name,{create:true},function(file){
				getFiles(fileSystem);
			},err);
		});
		socket.on('fragment',function(data){
			var frag = data.frag.split('/')
			fragmentQueue.enqueue({fileName:data.name,fileContent:data.data,fileFrag:frag[0],totalFrags:frag[1]});
		});
		socket.on('message',function(data){
			//$("#directory")[0].innerHTML=data.msg;
			if(data.type){
				fileSystem.root.getFile(data.name,{create:true,exclusive:true});
				$("#directory").html("File Type: "+data.type+"<br/>File Name: "+data.name+"<br/>================================<br/>");
			}
			$("#directory").html($("#directory").html()+data.data)
		});
		socket.on('me',function(data){
			myNode = data.name;
			sys.addNode(data.name,{'label':data.name+'\n(You)','radius':randrange(40,50),'color':color.random()})
		});
		socket.on('friends',function(data){
			sys.addNode(data.name,{'label':data.name,'radius':randrange(30,35),'color':color.random()});
			sys.addEdge(myNode,data.name);
		});
		socket.on('leave',function(data){
			console.log(data.name + " is leaving");
			sys.pruneNode(data.name);
		});
	});
})()