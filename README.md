###OnePush 
A socket based file sharing system. Built using Nodejs & Socket.IO

OnePush supports the following features:   
 - Establishing groups that are capable of sending and receiving files among themselves.   
 - Every file transfer is done in fragments over a socket connection   
 - The clients can upload utmost 1 file at a time and receive any number of files from the group simultaneously. This is done to prevent congestion of the network   
 - All transferred files are stored at the client's file system using the Javascript FileSystem API (The application reserves 300MB on the client machine)   
 
Features yet to be implemented:   
 - Making the transfers more reliable (by including ACK mechanisms)   