var express = require('express');
var app = express();
var io  = require('socket.io').listen(app.listen(8080),{log:false});

app.set('views', __dirname + '/view');
app.set('view engine', "jade");
app.engine('jade', require('jade').__express);
app.use(express.static(__dirname+'/public'));

app.get("/", function(req, res){
	res.render("page");
});
var Clients     = {};
var clientNames = {};
var userGroup   = {};
io.sockets.on('connection', function (client) {
	client.on('join',function(data){
		var id = data.id;
		client.clientName = data.name;
		if(typeof(Clients[id])=='undefined'){
			Clients[id]=[];
		}
		client.emit('me',{name:client.clientName});
		for(var i in Clients[id]){
			Clients[id][i].emit('friends',{name:client.clientName});
			client.emit('friends',{name:Clients[id][i].clientName});
		}
		Clients[id].push(client);
		userGroup[client.id] = id;
	});
	client.on('set-nick',function(data){
		console.log('request received')
		if(clientNames[data.name]) client.emit('set-nick-result',{status:false});
		else{
			clientNames[data.name]=1;
			client.emit('set-nick-result',{status:true});
		}
	});
	client.on('message',function(data){
		var group = userGroup[client.id];
		for(var i in Clients[group]){
			console.log("sent to "+Clients[group][i].clientName);
			Clients[group][i].emit('message',data);
		}
	});
	client.on('fragments',function(data){
		//console.log("Received #"+data.frag+"/"+data.total);
		var curr = userGroup[client.id];
		for(var i in Clients[curr]){		
			if(Clients[curr][i] != client){
				if(data.type){
					Clients[curr][i].emit('receive-new',{"type":data.type,"name":data.name});
					console.log("Emitting new file.. @"+Clients[curr][i].clientName);
				}
				Clients[curr][i].emit('fragment',data)
			}
		}
	})
	client.on('disconnect',function(){
		clientNames[client.clientName]=0;
		var curr = userGroup[client.id];
		for(var i in Clients[curr]){
			if(Clients[curr][i].id == client.id){
				console.log(client.clientName + " is leaving");
				Clients[curr].splice(i,1);
			}
			else{
				Clients[curr][i].emit('leave',{name:client.clientName});
			}
		}
	});
});